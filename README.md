// this is a code written in SC for the 8 channel piece. non interactive. it contains few sections, strange LFO that morphs into kicks, kicks which speed up, surround kicks, etc. written with SC forum help and Roc Jimenez de Cisneros. 


(

// this first bit is the SynthDef, the recipe for the kick synth we're using in the second part.


SynthDef(\gabberkickChan, { arg chan = 0, amp = 0.1;
     var snd; 
     snd = SinOsc.ar(Env([1000, 69, 60], [0.015, 0.1], \exp).ar * [-0.1, 0, 0.1].midiratio); 
     snd = Mix(snd); 
     snd = (snd * \drive.kr(10)).tanh; 
     snd = (snd * \drive.kr(10) * 0.5).atan; 
     snd = BPeakEQ.ar(snd, XLine.kr(2000, 100, 0.3), 2.0, 8); 
     snd = snd + DelayC.ar(snd, 0.01, SinOsc.ar(4).range(0.0, 0.001)); 
     snd = RLPF.ar(snd, 8000, 0.7); 
     snd = snd * Env([0, 1, 0.7, 0.7, 0], [0.001, 0.01, 0.3, 
0.02]).ar(2); 
     snd = (snd * 0.6).clip(-1, 1); 
     Out.ar(chan, snd * amp); 
}).add; 
 

/////// ok, from here on, this is where the actual Task starts, it simply keeps it in memory. you still need to execute the very last line below this block, the one saying "~eTask.start;" which starts the Task.

~eTask = Task({
	
// here you tell the Task how many times you want to repeat whatever you put inside it. if instead of "1.do" you write "inf.do", it would loop it infinitely. 

    inf.do({ 

// here is where the first part starts, LFOey sounds.  because the LFOey part is quite random in essence (all of those LFOs are pretty much random) it's repeated few times, in this case 7 times, and there' ll be 7 different results. this LFOey bit has 2 parts, each of those prints "PART 1".postln; or "PART 2".postln; whenever they get executed. 
 
////////////////////////////////////////////////////////////

////      ////////   ////////   ////////  ////    ////
////      ////////   ///  ///   ////////   ////  ////
////      ////       ///  ///   ////        ////////
////      ////////   ///  ///   ////////      ////
////////  ////       ///  ///   ////          ////
////////  ////       ////////   ////////      ////

////////////////////////////////////////////////////////////

7.do({ arg i; //7
["cycle", i].postln;
"PART 1".postln;


~lfo1 = {
	var base_freq = LFNoise2.kr(1).exprange(10, 60);
	var meat = PanAz.ar(7, SinOsc.ar( 
			EnvGen.ar(
				Env(
					[ 
						base_freq,
						LFNoise2.kr(1).range(40, LFNoise2.kr(0.1).range(500, 5000)),
						LFNoise2.kr(1!8).exprange(35, 125),
						base_freq
					],
					[
						LFNoise2.kr(1).exprange(0.001,0.08),
						LFNoise2.kr(1).exprange(0.008, [1, 0.4, 0.2, 2].choose),
						LFNoise2.kr(1).exprange(0.01,1.5)
					], 
					[
						LFNoise2.kr(1).exprange(1/5,5), 
						LFNoise2.kr(1).range(-19,-6), 
						LFNoise2.kr(1).range(-5,5)
					]
				).circle
			), 0,
			LFNoise2.kr(LFNoise2.kr(100).exprange(1/9,9)).range(0.05,1)
		),LFTri.ar(
			LFNoise2.kr(1/10).exprange(1/10,10), [0,2], 
			Saw.kr(LFNoise2.kr(1/10!2).exprange(1/5,5)).range(0.3,1)
		)
	).mean;
Clip.ar(meat, 0, 0.02) * 30		

}.play;

(5.0.rand + 5 + i).wait; // wait time for this block until the Task jumps to the next one! 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

"PART 2".postln;
~lfo1.release; // stop the  section!

~lfo2 = {
	var base_freq = LFNoise2.kr(1).exprange(10, 60);
	var meat = PanAz.ar(7,SinOsc.ar( 
			EnvGen.ar(
				Env(
					[ 
						base_freq,
						LFNoise2.kr(0.1).range(40, LFNoise2.kr(0.1).range(220, 1000)),
						LFNoise2.kr(0.1!8).exprange(35, 125),
						base_freq
					],
					[
						LFNoise1.kr(0.001).exprange(0.01, 1),
						LFNoise1.kr(0.002).exprange(0.08, [1, 0.4, 0.2, 0.7].choose),
						LFNoise2.kr(0.001).exprange(0.01, 0.041)
					], 
					[
						LFNoise2.kr(0.11).exprange(1/5,5), 
						LFNoise2.kr(0.11).range(-19,-6), 
						LFNoise2.kr(0.1).range(-5,5)
					]
				).circle
			), 0,
			LFNoise2.kr(LFNoise2.kr(100).exprange(1/9,9)).range(0.05,1)
		), LFTri.ar(
			LFNoise2.kr(1/20).exprange(1/10,10), [0,2], 
			LFSaw.kr(LFNoise2.kr(1/10!2).exprange(1/5,5)).range(0.3,1)
		)
	).mean;
Clip.ar(meat, 0, 0.02) * 30		

}.play;

(10 + 5.0.rand).wait; // wait time for this block until the Task jumps to the next one!

~lfo2.release; // stop the  section!

    });


~lfo2 = {
	var base_freq = LFNoise2.kr(1).exprange(10, 60);
	var meat = PanAz.ar(7, SinOsc.ar( 
			EnvGen.ar(
				Env(
					[ 
						base_freq,
						LFNoise2.kr(111).range(333, 111),
						5,
						base_freq
					],
					[
						0.001,
						LFNoise2.kr(111).range(0.1, 0.8),
						0.001
					], 
					[
						LFNoise2.kr(0.11).range(1,5), 
						LFNoise2.kr([55, 66, 77, 88, 41, 22, 65, 16]).range(-3, -8), 
						LFNoise2.kr(0.1).range(1, 5)
					]
				).circle
			), 0,
			LFNoise2.kr(LFNoise2.kr(100).exprange(1/9,9)).range(0.05,1)
		), LFTri.ar(
			LFNoise2.kr(1/20).exprange(1/10,10), [0,2], 
			Saw.kr(LFNoise2.kr(1/10!2).exprange(1/5,5)).range(0.3,1)
		)
	).mean;
Clip.ar(meat, 0, 0.02) * 30		

}.play;

80.wait; //50

~lfo2.release; 

////////////////////////////////////////////////////////////
////  ///    ////   ////////   ////  ///     ///////
//// ///     ////   ///////    ///// ///    ////
///////      ////   ///        ///////       /////
////\\\      ////   ///        ////\\\          /// 
////  \\\    ////   ////////   ////  \\\    ////////
////    \\\  ////   ////////   ////    \\\  ///////
/////////////////////////////////////////////////////////////

// this is where the crazy kicks start.
// this is composed of 4 different sections, marked ~kick1 to ~kick9. 

// this first section simply plays with duration, going from slow to faster: Array.interpolation()

"kick1".postln;
~kick1 = Pbind(\instrument, \gabberkickChan, 
	 \chan, (0..7),
     \amp, 0.6,
     \dur, Pseq(Array.interpolation(100, 2, 0.01).mirror, inf), 
     \drive, Pseq(Array.series(133, 0, 1 / 133).linexp(0, 1, 1, 
10).mirror, inf) 
).play;  

100.wait; // wait time for this section.

~kick1.stop; 

// now ~kick2, the second kick section! very subtle differences in duration, so they drift apart a bit because of that Pbrown that we add to the steady 0.3 duration for each step.


"kick2".postln;
~kick2_ch0.stop; ~kick2_ch0 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 0,
     \amp, 0.6, 
     \dur, Pseq([0.3], inf) + Pbrown(0, 0.01, 0.001, inf), 
     \drive, Pseq(Array.series(40, 0, 1 / 40).linexp(0, 1, 1, 
10).mirror, inf) 
).play;

10.wait; 


~kick2_ch1.stop; ~kick2_ch1 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 1,
     \amp, 0.6, 
     \dur, Pseq([0.3], inf) + Pbrown(0, 0.01, 0.001, inf), 
     \drive, Pseq(Array.series(30, 0, 1 / 30).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

10.wait; 

~kick2_ch2.stop; ~kick2_ch2 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 2,
     \amp, 0.6, 
     \dur, Pseq([0.3], inf) + Pbrown(0, 0.01, 0.001, inf), 
     \drive, Pseq(Array.series(66, 0, 1 / 66).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

10.wait; 

~kick2_ch3.stop; ~kick2_ch3 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 3,
     \amp, 0.6, 
     \dur, Pseq([0.3], inf) + Pbrown(0, 0.01, 0.001, inf), 
     \drive, Pseq(Array.series(30, 0, 1 / 30).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

10.wait; 

~kick2_ch4.stop; ~kick2_ch4 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 4,
     \amp, 0.6, 
     \dur, Pseq([0.3], inf) + Pbrown(0, 0.01, 0.001, inf), 
     \drive, Pseq(Array.series(22, 0, 1 / 22).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

10.wait; 

~kick2_ch5.stop; ~kick2_ch5 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 5,
     \amp, 0.6, 
     \dur, Pseq([0.3], inf) + Pbrown(0, 0.01, 0.001, inf), 
     \drive, Pseq(Array.series(55, 0, 1 / 55).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

10.wait; 

~kick2_ch6.stop; ~kick2_ch6 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 6,
     \amp, 0.6, 
     \dur, Pseq([0.3], inf) + Pbrown(0, 0.01, 0.001, inf), 
     \drive, Pseq(Array.series(88, 0, 1 / 88).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

10.wait; 

~kick2_ch7.stop; ~kick2_ch7 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 7,
     \amp, 0.6, 
     \dur, Pseq([0.3], inf) + Pbrown(0, 0.01, 0.001, inf), 
     \drive, Pseq(Array.series(74, 0, 1 / 74).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 


50.wait; // wait time


// now ~kick3: part 3 of the kicks, where the duration varations are even smaller. 
// take a look at the Pbrown in the previous section: Pbrown(0, 0.01, 0.001, inf) that means minimum value is 0, maximum is 0.01 and the maximum jump betwen steps is 0.001. now for this ~kick3 section, the Pbrown is as follows: Pbrown(0, 0.001, 0.001, inf), so the maximum value is way smaller, hence all kicks on all channels stay much closer. 
// again, rather than one supercool Pbind here are 8 of them, for channels 0 to 7:

"kick3".postln;

~kick3_ch0.stop; ~kick3_ch0 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 0,
     \amp, 0.6, 
     \dur, Pseq([0.5], inf) + Pbrown(0, 0.001, 0.001, inf), 
     \drive, Pseq(Array.series(88, 0, 1 / 88).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

~kick3_ch1.stop; ~kick3_ch1 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 1,
     \amp, 0.6, 
     \dur, Pseq([0.5], inf) + Pbrown(0, 0.001, 0.001, inf), 
     \drive, Pseq(Array.series(122, 0, 1 / 122).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

~kick3_ch2.stop; ~kick3_ch2 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 2,
     \amp, 0.6, 
     \dur, Pseq([0.5], inf) + Pbrown(0, 0.001, 0.001, inf), 
     \drive, Pseq(Array.series(133, 0, 1 / 133).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

~kick3_ch3.stop; ~kick3_ch3 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 3,
     \amp, 0.6, 
     \dur, Pseq([0.5], inf) + Pbrown(0, 0.001, 0.001, inf), 
     \drive, Pseq(Array.series(96, 0, 1 / 96).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

~kick3_ch4.stop; ~kick3_ch4 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 4,
     \amp, 0.6, 
     \dur, Pseq([0.5], inf) + Pbrown(0, 0.001, 0.001, inf), 
     \drive, Pseq(Array.series(112, 0, 1 / 112).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

~kick3_ch5.stop; ~kick3_ch5 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 5,
     \amp, 0.6, 
     \dur, Pseq([0.5], inf) + Pbrown(0, 0.001, 0.001, inf), 
     \drive, Pseq(Array.series(130, 0, 1 / 130).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

~kick3_ch6.stop; ~kick3_ch6 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 6,
     \amp, 0.6, 
     \dur, Pseq([0.5], inf) + Pbrown(0, 0.001, 0.001, inf), 
     \drive, Pseq(Array.series(230, 0, 1 / 230).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

~kick3_ch7.stop; ~kick3_ch7 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 7,
     \amp, 0.6, 
     \dur, Pseq([0.5], inf) + Pbrown(0, 0.001, 0.001, inf), 
     \drive, Pseq(Array.series(99, 0, 1 / 99).linexp(0, 1, 1, 
10).mirror, inf) 
).play; 

100.wait; // wait time


// we stop all instances of kick3 before moving on to the next bit:



~kick3_ch0.stop; 
~kick3_ch1.stop; 
~kick3_ch2.stop; 
~kick3_ch3.stop; 
~kick3_ch4.stop; 
~kick3_ch5.stop; 
~kick3_ch6.stop; 
~kick3_ch7.stop; 
~kick2_ch0.stop; 
~kick2_ch1.stop; 
~kick2_ch2.stop; 
~kick2_ch3.stop; 
~kick2_ch4.stop; 
~kick2_ch5.stop; 
~kick2_ch6.stop; 
~kick2_ch7.stop; 



// now ~kick4, last section, where kicks jump from channel to channel in a random way, 
// that's because we added that "\chan, Prand((0..7), 100)"

"kick4".postln;

 ~kick4 = Pbind(\instrument, \gabberkickChan, 
	 \chan, Prand((0..7), inf),
     \amp, Pbrown(0.5, 1, 0.1, inf), 
     \dur, 0.5,    
 \drive,2,

).play;

60.wait; // wait time
~kick4.stop; 

// so on
"kick5".postln;

 ~kick5 = Pbind(\instrument, \gabberkickChan, 
	 \chan, Prand((0..7), inf),
     \amp, Pbrown(0.5, 1, 0.1, inf), 
     \dur, 0.25,    
 \drive,Pseq(Array.series(99, 0, 1 / 99).linexp(0, 1, 1, 
10).mirror, inf) 

).play;


60.wait; // wait time
~kick5.stop;

"kick6".postln;

 ~kick6 = Pbind(\instrument, \gabberkickChan, 
	 \chan, Prand((0..7), inf),
     \amp, Pbrown(0.5, 1, 0.1, inf), 
     \dur, 0.15,    
 \drive,Pseq(Array.series(99, 0, 1 / 99).linexp(0, 1, 1, 
10).mirror, inf)
).play;


60.wait; // wait time
~kick6.stop;

 
"kick7".postln;
~kick7 = Pbind(\instrument, \gabberkickChan, 
	 \chan, Prand((0..7), inf),
     \amp, Pbrown(0.5, 1, 0.1, inf), 
     \dur, 0.04,    
 \drive,Pseq(Array.series(99, 0, 1 / 99).linexp(0, 1, 1, 
10).mirror, inf)
).play;


60.wait; // wait time
~kick7.stop; 

"kick8".postln;

~kick8 = Pbind(\instrument, \gabberkickChan, 
	 \chan, Prand((0..7), inf),
     \amp, Pbrown(0.5, 1, 0.1, inf), 
     \dur, 0.01,    
 \drive,3

).play;


60.wait; // wait time
~kick8.stop; 


// gabber section
"kick9".postln;
~kick9_ch0.stop; ~kick9_ch0 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 0,
     \amp, 0.6, 
     \dur,  Pseq ([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.25, 0.25], inf),
\f2, Pseq([44, 55, 66, 77, 88, 99, 111].stutter(8), inf),
     \drive, Pseq(Array.series(40, 0, 1 / 40).linexp(0, 1, 1, 
10).mirror, inf) 
).play;

15.wait;


~kick9_ch1.stop; ~kick9_ch1 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 1,
     \amp, 0.6, 
     \dur,  Pseq ([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.25, 0.25], inf),
\f2, Pseq([44, 55, 66, 77, 88, 99, 111].stutter(8), inf),
     \drive, 2, 
).play;

15.wait;

~kick9_ch2.stop; ~kick9_ch2 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 2,
     \amp, 0.6, 
     \dur,  Pseq ([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.25, 0.25], inf),
\f2, Pseq([44, 55, 66, 77, 88, 99, 111].stutter(8), inf),
     \drive, Pseq(Array.series(40, 0, 1 / 40).linexp(0, 1, 1, 
10).mirror, inf) 
).play;

15.wait;

~kick9_ch3.stop; ~kick9_ch3 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 3,
     \amp, 0.6, 
     \dur,  Pseq ([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.25, 0.25], inf),
\f2, Pseq([44, 55, 66, 77, 88, 99, 111].stutter(8), inf),
     \drive, 3,
).play;

15.wait;

~kick9_ch4.stop; ~kick9_ch4 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 4,
     \amp, 0.6, 
     \dur,  Pseq ([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.25, 0.25], inf),
\f2, Pseq([44, 55, 66, 77, 88, 99, 111].stutter(8), inf),
     \drive, Pseq(Array.series(40, 0, 1 / 40).linexp(0, 1, 1, 
10).mirror, inf) 
).play;

15.wait;

~kick9_ch5.stop; ~kick9_ch5 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 5,
     \amp, 0.6, 
     \dur,  Pseq ([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.25, 0.25], inf),
\f2, Pseq([44, 55, 66, 77, 88, 99, 111].stutter(8), inf),
     \drive, Pseq(Array.series(40, 0, 1 / 40).linexp(0, 1, 1, 
10).mirror, inf) 
).play;

15.wait;

~kick9_ch6.stop; ~kick9_ch6 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 6,
     \amp, 0.6, 
     \dur,  Pseq ([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.25, 0.25], inf),
\f2, Pseq([44, 55, 66, 77, 88, 99, 111].stutter(8), inf),
     \drive, Pseq(Array.series(40, 0, 1 / 40).linexp(0, 1, 1, 
10).mirror, inf) 
).play;

15.wait;

~kick9_ch7.stop; ~kick9_ch7 = Pbind(\instrument, \gabberkickChan, 
	 \chan, 7,
     \amp, 0.6, 
     \dur,  Pseq ([0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.25, 0.25], inf),
\f2, Pseq([44, 55, 66, 77, 88, 99, 111].stutter(8), inf),
     \drive, 1,
).play;

50.wait;

~kick9_ch0.stop; 
~kick9_ch1.stop; 
~kick9_ch2.stop; 
~kick9_ch3.stop; 
~kick9_ch4.stop; 
~kick9_ch5.stop; 
~kick9_ch6.stop; 
~kick9_ch7.stop; 




  });
});
)


// RUN THE TASK WITH THIS LINE:
~eTask.start;
